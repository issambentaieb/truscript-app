import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import  { DirectionsPage } from '../directions/directions';
/**
 * Generated class for the VoucherPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-voucher',
  templateUrl: 'voucher.html',
})
export class VoucherPage {

  gpi : any ;
  searching : any ;
  //pharmacy info
  name:any ;
  address:any ;
  city: any ;
  zip:any ;
  phone:any;
  distance:any;

  lon:any;
  lat:any;
  zipCode:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad VoucherLocationPage');


    this.gpi = this.navParams.get('gpi');
    this.searching = this.navParams.get('searching');
    this.name = this.navParams.get('name');
    this.address = this.navParams.get('address');
    this.city = this.navParams.get('city');
    this.zip = this.navParams.get('zip');
    this.phone = this.navParams.get('phone');
    this.lon = this.navParams.get('lon');
    this.lat = this.navParams.get('lat');
    this.distance = this.navParams.get('distance');
    this.zipCode = this.navParams.get('zipcode');
  }
  goToDirections(){
    this.navCtrl.push(DirectionsPage) ;
  }

}
