import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

//import  { CacheService } from "ionic-cache";

//import { HttpModule } from '@angular/http';
import 'rxjs/add/operator/map';
import {SearchConfigPage} from '../search-config/search-config' ;


/**
 * Generated class for the SearchBarPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-search-bar',
  templateUrl: 'search-bar.html',
})
export class SearchBarPage {
  search = '';
  //recent searches
  recent: any ;
//  public gpi =0 ;
	items: any;
	//PARTIAL_DRUG_NAME : string ; //replaced partialdrugname with searching
  //response: any ;


  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, private storage:Storage/*, private cache: CacheService*/) {

//   this.items = new Set();
this.items = new Set() ;
//this.ionViewDidLoad();

  }


//Called whenever a new search is typed
loadValues(){


  this.items.clear();


  var url='http://chiron.rxbert.com/api/v1/drug/search?api_key=5a9a2a5eca1a4af636ba1c9b6bfebcfe&partial_name='+this.search;
  //console.log(url) ;

        this.http.get(url).map(res => res.json()).subscribe(data => {
         for (var i=0; i<data.data.length; i++) {


       //  console.log(data.data[i].ProductNameAbr);
       var n1 = data.data[i].ProductNameAbr ;
       this.items.add(n1);


        //  var url2 = 'http://chiron.rxbert.com/api/v1/drug/info?api_key=5a9a2a5eca1a4af636ba1c9b6bfebcfe&gpicode='+data.data[i].GPICode ; //3rd link in documentation
        //  console.log(url2);
        //
        //  this.http.get(url2).map(res => res.json()).subscribe(data2 => {
        //
        //    console.log (data2) ;
        //
        //  for (var brandOrGenetic in data2.data.drugs) {
        //    for (var dosage in data2.data.drugs[brandOrGenetic] ) {
        //      for (var form in data2.data.drugs[brandOrGenetic][dosage]){
        //        for( var type in data2.data.drugs[brandOrGenetic][dosage][form]) {
        //        for (var quantity in data2.data.drugs[brandOrGenetic][dosage][form][type] ){
        //
        //          console.log(data2.data.drugs[brandOrGenetic][dosage][form][type][quantity]["ProductNameAbr"] + '!=' +n1);
        //
        //          console.log(data2.data.drugs[brandOrGenetic][dosage][form][type][quantity]["ProductNameAbr"].trim() === n1.trim() );
        //
        //          //TODO find a string.equals equivalent
        //         if( data2.data.drugs[brandOrGenetic][dosage][form][type][quantity]["ProductNameAbr"].trim() === n1.trim() )
        //         {
        //         this.items.add(n1+' ('+ brandOrGenetic+')') ;
        //         console.log(n1+' ('+ brandOrGenetic+')') ;
        //       }
        //     }
        //       }}}}});
          }

        });


}

setRecent()
{
  this.ionViewDidLoad();
}

//testing for console
loadList() {
 this.http.get('http://chiron.rxbert.com/api/v1/drug/info?api_key=5a9a2a5eca1a4af636ba1c9b6bfebcfe&gpicode=39400010100310').map(res=> res.json()).subscribe(data => {
   console.log(data); //    this.posts = data.data.children;
})/* ; */   ;
}

 ;
 //sending the searched drug to the refined configuration page "search-config"
goToSearchConfigPage(selectedFromTheList) {
//  console.log('THIS IS WHAT I SEN4');

  //console.log(this.items)
  //console.log(this.items.entries().next());
  var gpi ;
  console.log(selectedFromTheList) ;


   //looking for the gpi code for the drug
   var url='http://chiron.rxbert.com/api/v1/drug/search?api_key=5a9a2a5eca1a4af636ba1c9b6bfebcfe&partial_name='+ selectedFromTheList;
  // console.log(url) ;
   //console.log(selectedFromTheList);
         this.http.get(url).map(res => res.json()).subscribe(data => {
          for (var i=0; i<data.data.length; i++) {


            //TODO fix string comparisson problem
       if (data.data[i].ProductNameAbr==selectedFromTheList)    {  gpi=data.data[i].GPICode;
                          //       console.log(data.data[i].GPICode) ;
                          //       console.log(gpi);
                                 this.navCtrl.push(SearchConfigPage,{'search':selectedFromTheList ,'gpi': gpi}) ;
                                 break ;
 }
 }

     });



}
ionViewDidLoad(){

    this.recent = new Set();

    this.storage.get("recent1").then((data)=>{
      if (data!=null){
       this.recent.add(data);
    //   console.log('Added the first one'+data);

     }
     });

       this.storage.get("recent2").then((data)=>{
         if (data!=null){
          this.recent.add(data);
      //    console.log('added the second one'+data);

        }
        });

          this.storage.get("recent3").then((data)=>{
            if (data!=null){
             this.recent.add(data) ;
          //  console.log('added the third one'+data);
           }
           });

}
}
