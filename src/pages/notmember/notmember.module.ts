import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotmemberPage } from './notmember';

@NgModule({
  declarations: [
    NotmemberPage,
  ],
  imports: [
    IonicPageModule.forChild(NotmemberPage),
  ],
  exports: [
    NotmemberPage
  ]
})
export class NotmemberPageModule {}
