import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { SearchPage } from '../search/search' ;
import { NotmemberPage } from '../notmember/notmember' ;

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login-page',
  templateUrl: 'login-page.html',
})

export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

// identification
//Will be "2way binded" using the [(ngModel)]=
username = '';
password = '';

get score()
{
const letters = (this.username + this.password).toLowerCase() ;
let sum = 0 ;
for (let i=0 ; i<letters.length;i++)
sum += letters.charCodeAt(i);

return sum % 100;
}



alertFunction(t :string,s :string)
{
     let alert = this.alertCtrl.create({
      title: t,
      subTitle: s,
      buttons: ['OK']
    });
    alert.present();

  }


//For when wrong or missing profile information
 login() {


  let t = '';
  let s ='' ;

  if (this.username == '' && this.password == '') {s ='Please provide username and passeword' ;t='Missing Information'; this.alertFunction(t,s);}
  else {
    if (this.username == '') {s='Please provide username' ; t='Missing Information'; this.alertFunction(t,s);}
    else {
  if (this.password == '') {s='Please provide Password' ;t='Missing Information';this.alertFunction(t,s);}


  else {  this.login1();}
  }}


}


  //simple navigation for now
  login1(){

  this.navCtrl.push(SearchPage) ;
  }

  //Redirection to 'not a member?' disclaimer
  notmember (){
this.navCtrl.push(NotmemberPage) ;
  }

}
