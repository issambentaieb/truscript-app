import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import { SearchPage } from '../search/search' ;
import {VoucherLocationPage} from '../voucher-location/voucher-location' ;
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
//import { Geolocation } from '@ionic-native/geolocation';


/**
 * Generated class for the SearchConfigPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-search-config',
  templateUrl: 'search-config.html',
})
export class SearchConfigPage {

//full name of drug in question
searching : string ;
//partial name of drug in question
search : string ;
gpi : any ;
ndc:any ;


 drugNames : any ;
 drugTypes: any ;
 drugDosages: any;
 drugForms: any;
 drugQtes: any ;

//The selected parameters
 drugType :any ;
 drugDosage :any ;
 drugQte : any ;
 drugForm : any ;
 zipCode : any ;

//position
 lat : any ;
 lon : any ;

 productID:any ;
  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController,public http: Http,private storage: Storage,private geolocation: Geolocation) {
//Filling-up with the options

this.ionViewDidLoad();
//console.log(this.gpi);
//console.log(this.searching);
this.drugNames = new Set() ;
this.drugTypes = new Set() ;
this.drugDosages = new Set();
this.drugForms = new Set();
this.drugQtes = new Set();

//console.log(this.searching);


var url1 = 'http://chiron.rxbert.com/api/v1/drug/info?api_key=5a9a2a5eca1a4af636ba1c9b6bfebcfe&gpicode='+this.gpi ; //3rd link in documentation

this.http.get(url1).map(res => res.json()).subscribe(data2 => {
    //4-Select dosage of drug : Checking dosage in both generic and brandOrGenetic options
//console.log(data2);

//This is where all the magic happens - loops, loops everywhere!
    for (var brandOrGenetic in data2.data.drugs) {
      for (var form in data2.data.drugs[brandOrGenetic] ) {

        if (!(form in this.drugForms ))
         this.drugForms.add(form) ;

     for (var packtyp in data2.data.drugs[brandOrGenetic][form])
     {
       //var dosagePlus = dosage + ' ('+form+')' ;

    //Filling in drug dosages available
     if (!(packtyp in this.drugTypes ))
      this.drugTypes.add(packtyp) ;

      for( var dosage in data2.data.drugs[brandOrGenetic][form][packtyp])
      {
        //Filling in drug types

        if (!(dosage in this.drugDosages))
        this.drugDosages.add(dosage);

          for (var quantity in data2.data.drugs[brandOrGenetic][form][packtyp][dosage] )

          { //Filling quantities of drugs available
             var q = quantity ;//+ ' ' + form ;
             if (!(q in this.drugQtes))
             this.drugQtes.add(q) ;

            // console.log(data2.data.drugs[brandOrGenetic][dosage][form][type][quantity]["ProductNameAbr"]);
             //console.log(brandOrGenetic);

               var nom = data2.data.drugs[brandOrGenetic][form][packtyp][dosage][quantity]["ProductNameAbr"] ;

                //Filling in drug names (generic/brand )


             if (!(nom in this.drugNames))
               {
              this.drugNames.add(nom) ;

            }
          }

      }

    }

}
}

//init

//console.log(this.searching);
//console.log(this.drugType);

// ZipCode search

this.geolocation.getCurrentPosition().then((resp) => {
  this.lat = resp.coords.latitude;
  this.lon =resp.coords.longitude ;
 //console.log(latitude) ;
 //console.log(longitude);
var GEOurl = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='+this.lat+','+this.lon+'&sensor=true' ;
console.log(GEOurl) ;
this.http.get(GEOurl).map(res => res.json()).subscribe(data => {
  try{
this.setZipCode(data.results[0].address_components[7].long_name);
}
catch (e)
{
  console.log(e);
  this.setZipCode(72703)

}
console.log(this.zipCode);
}) ;
}) ;

console.log(this.zipCode);

//init to the lowest value
this.searching = this.drugNames.entries().next().value[0] ;
this.drugDosage = this.drugDosages.entries().next().value[0] ;
this.drugQte = this.drugQtes.entries().next().value[0] ;
 this.drugType = this.drugTypes.entries().next().value[0] ;
this.drugForm = this.drugForms.entries().next().value[0] ;

console.log(this.drugQte);

// this.searching = this.drugNames
  });

  console.log(this.drugNames);
  }

  //uses the API to dynamically fill out the config sets with options appropriate to the ones currently in selection
  loadDynamicOptions()
  {

    //Empty out the existing lists
    this.drugTypes.clear();
    this.drugDosages.clear();
    this.drugQtes.clear();


    var url1 = 'http://chiron.rxbert.com/api/v1/drug/info?api_key=5a9a2a5eca1a4af636ba1c9b6bfebcfe&gpicode='+this.gpi ; //3rd link in documentation

    this.http.get(url1).map(res => res.json()).subscribe(data2 => {

      // var initType;
      // var initDosage;
      // var initQte;

      for (var brandOrGenetic in data2.data.drugs) {
        for (var form in data2.data.drugs[brandOrGenetic] ) {
          for (var packtyp in data2.data.drugs[brandOrGenetic][form]) {
            for( var dosage in data2.data.drugs[brandOrGenetic][form][packtyp]){
              for (var quantity in data2.data.drugs[brandOrGenetic][form][packtyp][dosage] ){



                 var nom = data2.data.drugs[brandOrGenetic][form][packtyp][dosage][quantity]["ProductNameAbr"] ;



                 if (nom == this.searching && form == this.drugForm )

                {


                  if (!(packtyp in this.drugTypes)) {
                    //Init with the first value
                //    if (this.drugTypes.size == 0 ) initType = packtyp ;
                    this.drugTypes.add(packtyp) ;

                    if (packtyp == this.drugType) {
                   if (!(dosage in this.drugDosages )) {

                     //Init
                  //   if (this.drugDosages.size==0) initDosage = dosage;
                      this.drugDosages.add(dosage);

                   if (!(quantity in this.drugQtes)) {
                      //Init
                    //  if (this.drugQtes.size == 0 ) initQte =quantity ;
                      this.drugQtes.add(quantity);
                  // console.log("Quantity"+ quantity + "for drug of type"+packtyp )
                //   console.log (nom +' '+ this.searching + ' ' + form+' '+this.drugForm +' '+ packtyp +' '+ this.drugType);
              }}}}}

                  //Filling in drug names (generic/brand )



            }    }
        }}}


    // this.drugQte = initQte ;
    // this.drugType= initType ;
    // this.drugDosage = initDosage
if (this.drugQtes.entries().next() == null) { this.drugDosage = this.drugDosages.entries().next().value[0] ; this.loadDynamicOptions();}

   try{
   this.drugDosage = this.drugDosages.entries().next().value[0] ;
    this.drugQte = this.drugQtes.entries().next().value[0] ;
  }
  catch (e)
  {
    this.drugType = this.drugTypes.entries().next().value[0] ; this.loadDynamicOptions() ;


  }

  // 1 this.drugForm = this.drugForms.entries().next().value[0] ;

  });

    //reseting init values
    // this.drugDosage = this.drugDosages.entries().next().value[0] ;


    console.log(this.drugQte);
  }


  ionViewDidLoad() {
    //console.log('ionViewDidLoad SearchConfigPage');
    this.search = this.navParams.get('search');//.toUpperCase();
    this.gpi = this.navParams.get('gpi');


    //save the recently searched drugs (3 spots)

        this.storage.get("rsize").then((rsize)=>{
          //no recent

//console.log('rsize is '+rsize);
          if (rsize==null) { this.storage.set("recent1",this.search) ;
                            this.storage.set("rsize",1);}
          //one recent
          if (rsize == 1) {
            this.storage.set("rsize",2);

            this.storage.get("recent1").then((r1)=>{
              this.storage.set("recent2",r1) ;
              this.storage.set("recent1",this.search);

            });
          }            //two recent or more (having 2 recnet is the same as having three)
          if (rsize>=2 ){
          //  console.log("i is here'") ;
            this.storage.set("rsize",3);
            this.storage.get("recent2").then((r2)=>{
            //  console.log('the thirs is now'+r2) ;
              this.storage.set("recent3",r2) ;

              this.storage.get("recent1").then((r1)=>{
                this.storage.set("recent2",r1) ;
                  //console.log('the 2nd is now'+r1) ;
                this.storage.set("recent1",this.search);
          //        console.log('the first is now'+this.search) ;
              });
            });
          }
  });







    //      if (data != null) {
    //     this.storage.set("recent3",data) ;
    //     this.storage.set("recent2",this.searching);
    //     }
    //     else{
    //
    //       this.storage.get("recent1").then((data)=>{
    //         if (data==null)
    //
    //         });
    //     this.storage.set("recent1",this.searching) ;
    //
    //   }



  }

  setZipCode(z){


    this.zipCode = z ;
    console.log('I have zet the zipcode!!!to '+this.zipCode)
  }

 goToSearchPage()
  {

    this.navCtrl.push(SearchPage) ;

  }


  //Alert to recieve durg quantity
  enterQte(){
    let prompt = this.alertCtrl.create({
      title: 'Enter Your quantity',
      message: "",
      inputs: [
        {
          name: 'title',
          placeholder: 'Title'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();
  }

 //Alert to recieve zip code
  presentZipcodePrompt(){
    let prompt = this.alertCtrl.create({
      title: 'Zip Code',
      message: "Please enter your Zip code",
      inputs: [
        {
          name: 'ZipCode',
          placeholder: 'Zip Code'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();
  }


goToVoucherLocation(){
    console.log("displaying the recieved info");
    console.log(this.searching);
    console.log(this.drugType);
    console.log(this.drugDosage);
    console.log(this.drugQte);

    var url1 = 'http://chiron.rxbert.com/api/v1/drug/info?api_key=5a9a2a5eca1a4af636ba1c9b6bfebcfe&gpicode='+this.gpi ; //3rd link in documentation

    var productID ;

    //Obtaining the productID of the drug relative to the chosen configuration
    this.http.get(url1).map(res => res.json()).subscribe(data2 => {


      //figuring out whether it's brand or generic

      //try-catch to bypass "undefined" field
 try{
      if (data2.data.drugs["brand"][this.drugForm][this.drugType][this.drugDosage][this.drugQte]["ProductNameAbr"] == this.searching) {
      productID = data2.data.drugs["brand"][this.drugForm][this.drugType][this.drugDosage][this.drugQte]["ProductID"] ;
      console.log(productID);
    }
    if (data2.data.drugs["generic"][this.drugForm][this.drugType][this.drugDosage][this.drugQte]["ProductNameAbr"] == this.searching ){
     productID = data2.data.drugs["generic"][this.drugForm][this.drugType][this.drugDosage][this.drugQte]["ProductID"] ;
     console.log(productID);
   }

  }
  catch (e){
    if (data2.data.drugs["generic"][this.drugForm][this.drugType][this.drugDosage][this.drugQte]["ProductNameAbr"] == this.searching ){
     productID = data2.data.drugs["generic"][this.drugForm][this.drugType][this.drugDosage][this.drugQte]["ProductID"] ;
     console.log(productID);
   }
}

finally {
    console.log(this.zipCode);
     this.navCtrl.push(VoucherLocationPage,{'searching':this.searching,'gpi':this.gpi,'lat':this.lat,'lon':this.lon,'productID':productID,'drugType':this.drugType,'drugForm':this.drugForm,'drugDosage':this.drugDosage,'drugQte':this.drugQte,'zipCode':this.zipCode})
}

});


}

selectDosage(id){

  console.log ('Dosage you chose is'+id);
}

selectType(){
  console.log(this.drugType);
}

}
