import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchConfigPage } from './search-config';

@NgModule({
  declarations: [
    SearchConfigPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchConfigPage),
  ],
  exports: [
    SearchConfigPage
  ]
})
export class SearchConfigPageModule {}
