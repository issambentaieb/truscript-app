import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VoucherLocationPage } from '../voucher-location/voucher-location';

@NgModule({
  declarations: [
    VoucherLocationPage,
  ],
  imports: [
    IonicPageModule.forChild(VoucherLocationPage),
  ],
  exports: [
    VoucherLocationPage
  ]
})
export class SearchConfigPageModule {}
