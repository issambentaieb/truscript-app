import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { SearchPage } from '../search/search' ;
import {PharmacyListPage } from '../pharmacy-list/pharmacy-list' ;
import { Storage } from '@ionic/storage';
import { VoucherPage } from '../voucher/voucher';

/**
 * Generated class for the VoucherLocationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-voucher-location',
  templateUrl: 'voucher-location.html',
})
export class VoucherLocationPage {

  searching ='...';
  pharmaciesList : any ;
  gpi : any;
  ndc : any;
  zipCode: any ;

  //position
  lon:any ;
  lat:any ;

  productID:any ;
  drugQte:any ;
  drugDosage:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public storage: Storage,public http: Http) {

    //Recieving the selected information about the drug
//this.ionViewDidLoad();

  this.ionViewDidLoad();
   //Acquiring the list of pharmacies
   var url1 = 'http://chiron.rxbert.com/api/v1/pharmacy/search?api_key=5a9a2a5eca1a4af636ba1c9b6bfebcfe&zipcode='+this.zipCode //3rd link in documentation
var p = new Array() ;
   this.pharmaciesList = new Set();
   this.http.get(url1).map(res => res.json()).subscribe( data => {
    // console.log( data);

     //Checking pricing for each of the pharmacies
     for (var i=0; i< data.data.pharmacies.length ; i++ ) {
          var npi = data.data.pharmacies[i].npi_id ;
          var n = data.data.pharmacies[i].name ;
          var address = data.data.pharmacies[i]['address_1'] ;
          var city = data.data.pharmacies[i]['city'];
          var zip = data.data.pharmacies[i]['zip'] ;
          var phone  = data.data.pharmacies[i]['phone'] ;
          var lon=data.data.pharmacies[i]['longitude'] ;
          var lat =data.data.pharmacies[i]['latitude'] ;
          var distance = this.getDistanceFromLatLonInMiles(lat,lon,this.lat,this.lon) ;

          var children = 0;
          if (data.data.pharmacies[i].children!=null) {

            children= data.data.pharmacies[i].children.length ;

            //if has one child. load the data into it and treat it as childless
            if (children ==1) {


               address = data.data.pharmacies[i]['children'][0]['address_1'] ;
               city = data.data.pharmacies[i]['children'][0]['city'];
               zip = data.data.pharmacies[i]['children'][0]['zip'] ;
               phone  = data.data.pharmacies[i]['children'][0]['phone'] ;
               lon=data.data.pharmacies[i]['children'][0]['longitude'] ;
               lat =data.data.pharmacies[i]['children'][0]['latitude'] ;
               distance = this.getDistanceFromLatLonInMiles(lat,lon,this.lat,this.lon) ;

              // console.log(data.data.pharmacies[i]['children'])
            //   console.log(data.data.pharmacies[i][0]['children'].zip)

               children =0 ;
            }
          }

        //    console.log(url2);
        //p contains a list of pharmacy name + it's npi
        //  console.log("The pharmacy number"+i+'has the npi '+npi + " and is called   "+n);
      var l = new Array() ;

      l.push(n) ;
      l.push(npi);
      l.push(children) ;
      l['address'] = address ;
      l['zip'] = zip ;
      l['city'] = city ;
      l['phone'] = phone ;
      l['distance']= distance +' Miles';

      p.push(l);
      //

      //console.log(l);
    }//



  //   console.log(p);
    var p1 = p.splice(6);
    //console.log(p);

//Now, all the pharmacies are loaded into p we need to "splice" p intou groups of 6 for optimisation purpuses
  while (p1.length >4)
  {
    this.loadPrice(p) ;
    p = p1.splice(6);
    var aux = [] ;
    aux = p ;
    p=p1 ;
    p1 = aux ;
//    console.log(p1);
  }
  //}

});
    //closing the first api call
    console.log("ok'");


}

  //  console.log(p);

//loads the prices of a set in a set of  pharmacies
  loadPrice(tab){

          //console.log('k is right now'+k) ;
          var url2 = 'http://chiron.rxbert.com/api/v1/drug/quote?api_key=5a9a2a5eca1a4af636ba1c9b6bfebcfe%20' ;
           ; // for now
           //console.log ("table is");
           //console.log (tab);

           for (var h=0; h<tab.length ; h++) {
             url2 += '&pharmacies[]='+tab[h][1] ;

      //    console.log('h is now'+h+ ' and the npi is '+tab[h][1]);
        }
          url2 += '&ndc='+this.productID+'&quantity='+this.drugQte ;
          //console.log(url2);
          //console.log('k is right now'+k) ;
          this.http.get(url2).map(res => res.json()).subscribe(data2 => {


        for (var h=0 ; h<tab.length ; h++){

          var npi = tab[h][1];
          //console.log ('npi is'+npi);
          var p = new Array()
        //  console.log(npi);
          p['name']=tab[h][0] ;
          p['price']=data2.data[npi]['PatientPayAmount']+'$' ;
          p['children']=tab[h][2] ;
          p['address'] = tab[h]['address'] ;
          p['zip'] = tab[h]['zip'] ;
          p['city'] = tab[h]['city'] ;
          p['phone'] = tab[h]['phone'] ;
          p['lat']=tab[h]['latitude'];
          p['lon']=tab[h]['longitude'];
          p['distance'] = tab[h]['distance']
          //console.log(p);
          this.pharmaciesList.add(p) ;

        }
        });
  }


  //for calculating distance
   getDistanceFromLatLonInMiles(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
  var dLon = this.deg2rad(lon2-lon1);
  var a =
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ;
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var d = R * c; // Distance in km
  return (d*0.621371).toFixed(2);
}

 deg2rad(deg) {
  return deg * (Math.PI/180)
}

  ionViewDidLoad() {
    //console.log('ionViewDidLoad VoucherLocationPage');

    this.searching = this.navParams.get('searching');//.toUpperCase();
    this.gpi = this.navParams.get('gpi');
    this.ndc = this.navParams.get('ndc');
    this.lon = this.navParams.get('lon');
    this.lat = this.navParams.get('lat');
    this.productID = this.navParams.get('productID');
    this.drugQte = this.navParams.get('drugQte');
    this.drugDosage = this.navParams.get('drugDosage');
    this.zipCode = this.navParams.get('zipCode');
  //  console.log(this.zipCode);

  }

  goToPharmacyList(name, price, children,address,city,zip,phone,distance)
  {
      if (children > 1 )
      this.navCtrl.push(PharmacyListPage,{'searching':this.searching,'gpi':this.gpi,'lat':this.lat,'lon':this.lon,'pharmacyName':name,'pharmacyPrice':price,'children':children,'city':city,'address':address,'zip':zip,'phone':phone,'zipCode':this.zipCode,'distance': distance,'drugDosage':this.drugDosage,'drugQte':this.drugQte} ) ;
      else
      this.navCtrl.push(VoucherPage,{'searching':this.searching,'gpi':this.gpi,'pharmacyName':name,'name':name,'pharmacyPrice':price,'city':city,'address':address,'zip':zip,'phone':phone,'zipCode':this.zipCode,'distance': distance}) ;

  }


  goToSearchPage()
   {

     this.navCtrl.push(SearchPage) ;

   }

}
