import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { VoucherPage } from '../voucher/voucher' ;
import { SearchPage } from '../search/search' ;
//import { Storage } from '@ionic/storage';
/**
 * Generated class for the PharmacyListPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-pharmacy-list',
  templateUrl: 'pharmacy-list.html',
})
export class PharmacyListPage {

  searching= '...';
  gpi : any ;
  drugQte : any ;
  drugDosage : any;
  pharmacyName : '' ;
  children : any ;
  zipCode: any;

  lon:any;
  lat:any;

  selectedPharmaciesList : any ;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http) {

    this.searching = this.navParams.get('searching');//.toUpperCase();
    this.gpi = this.navParams.get('gpi');
    this.children = this.navParams.get('children');
    this.pharmacyName = this.navParams.get('pharmacyName');
    this.lon = this.navParams.get('lon');
    this.lat = this.navParams.get('lat');
    this.zipCode = this.navParams.get('zipCode');
    this.drugQte =this.navParams.get('drugQte');
    this.drugDosage = this.navParams.get('drugDosage');



}


    ionViewDidLoad() {
      //console.log('ionViewDidLoad VoucherLocationPage');



      var url1 = 'http://chiron.rxbert.com/api/v1/pharmacy/search?api_key=5a9a2a5eca1a4af636ba1c9b6bfebcfe&zipcode='+this.zipCode //3rd link in documentation

      this.selectedPharmaciesList = new Set();
      this.http.get(url1).map(res => res.json()).subscribe(data => {
        console.log(data);
        for (var i=0 ; i<  data.data.pharmacies.length ; i++ )
         {
           console.log (data.data.pharmacies[i].name+' == '+this.pharmacyName)
           if (data.data.pharmacies[i].name == this.pharmacyName ) {
            for(var k=0; k<data.data.pharmacies[i]['children'].length ;k++) {

              let info = new Array() ;
              info ['name'] = this.pharmacyName ;
              info['address'] = data.data.pharmacies[i]['children'][k]['address_1'] ;
              info ['city'] = data.data.pharmacies[i]['children'][k]['city'];
              info ['zip'] = data.data.pharmacies[i]['children'][k]['zip'] ;
              info['phone'] = data.data.pharmacies[i]['children'][k]['phone'] ;
              var lon =data.data.pharmacies[i]['children'][k]['longitude'] ;
              var lat =data.data.pharmacies[i]['children'][k]['latitude']
              info['distance'] = this.getDistanceFromLatLonInMiles(lat,lon,this.lat,this.lon) +' Miles';
              console.log(info);
              this.selectedPharmaciesList.add(info);

            }
            break ;
          }


         }
 +' Miles'       //console.log(data);
      });


    }


    //for calculating distance
     getDistanceFromLatLonInMiles(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2-lon1);
    var a =
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c; // Distance in km
    return (d*0.621371).toFixed(2);
    }

    deg2rad(deg) {
    return deg * (Math.PI/180)
    }


  goToVoucher(name,address,city,zip,phone,distance){
    console.log('phone is '+phone+' but city is'+city);
      this.navCtrl.push(VoucherPage,{'city':city,'name':name,'lat':this.lat,'lon':this.lon,'address':address,'zip':zip,'searching':this.searching,'phone':phone,'distance':distance,'zipCode':this.zipCode}) ;
  }

  goToSearchPage()
   {

     this.navCtrl.push(SearchPage) ;

   }


}
