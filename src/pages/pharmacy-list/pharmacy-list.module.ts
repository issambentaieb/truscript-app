import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PharmacyListPage } from '../pharmacy-list/pharmacy-list';

@NgModule({
  declarations: [
    PharmacyListPage,
  ],
  imports: [
    IonicPageModule.forChild(PharmacyListPage),
  ],
  exports: [
    PharmacyListPage
  ]
})
export class SearchConfigPageModule {}
