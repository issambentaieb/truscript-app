import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
//import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation' ;

import { CacheModule } from "ionic-cache";
//import { Http } from '@angular/http';

import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login-page/login-page' ;
import { JapiProvider } from '../providers/japi/japi';
import { SearchPage } from '../pages/search/search';
import { SearchBarPage } from '../pages/search-bar/search-bar';
import  { NotmemberPage } from '../pages/notmember/notmember';
import { SearchConfigPage } from '../pages/search-config/search-config';
import { VoucherLocationPage } from '../pages/voucher-location/voucher-location';
import { PharmacyListPage } from '../pages/pharmacy-list/pharmacy-list';
import { VoucherPage } from '../pages/voucher/voucher';
import { DirectionsPage } from '../pages/directions/directions';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    SearchPage,
    SearchBarPage,
    //HttpModule,
    NotmemberPage,
    SearchConfigPage,
    VoucherLocationPage,
    PharmacyListPage,
    VoucherPage,
    DirectionsPage


  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    CacheModule.forRoot()

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    SearchPage,
    SearchBarPage,
    NotmemberPage,
    SearchConfigPage,
    VoucherLocationPage,
    PharmacyListPage,
    VoucherPage,
    DirectionsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    JapiProvider
  ]
})
export class AppModule {}
