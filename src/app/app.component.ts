import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CacheService } from "ionic-cache";
//import { Geolocation } from '@ionic-native/geolocation' ;

//import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login-page/login-page';
//import { SearchPage } from '../pages/search/search';
//import { SearchBarPage } from '../pages/search-bar/search-bar';
//import { NotmemberPage} from '../pages/notmember/notmember';
//import { SearchConfigPage } from '../pages/search-config/search-config';




@Component({
 //templateUrl: "build/app.html", may have been causeing cash problems
  template: `<ion-nav [root]="rootPage"></ion-nav>`
})
export class MyApp {
  rootPage:any = LoginPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,cache: CacheService) {
    platform.ready().then(() => {

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      cache.setDefaultTTL(60 * 60); //set default cache TTL for 1 hour
      splashScreen.hide();
    });
  }
}
